**Running Project Documentation**

First install the package "http-server" globally

- npm install http-server -g.

Then, just run

- http-server ./my-demo-app-prod

After that, visit http://localhost:8080 to view application